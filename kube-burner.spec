%define debug_package %{nil}

Name:           kube-burner
Version:        1.11.1
Release:        1%{?dist}
Summary:        Kube-burner is a tool aimed at stressing Kubernetes clusters by creating or deleting a high quantity of objects

License:        ASL 2.0
URL:            https://kube-burner.readthedocs.io/
Source0:        https://github.com/cloud-bulldozer/%{name}/releases/download/v%{version}/%{name}-V%{version}-Linux-x86_64.tar.gz

%description
Kube-burner is a tool aimed at stressing Kubernetes clusters 
by creating or deleting a high quantity of objects

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin/

%files
%license LICENSE
/usr/bin/kube-burner

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Mar 21 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.10.0

* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.9.4

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.8.1

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.7.12

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.7.6

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.7.3

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.7.1

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.6

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.5

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.3

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.0

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.17.1

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.16.3

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
